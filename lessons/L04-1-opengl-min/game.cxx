#include <algorithm>
#include <array>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <memory>
#include <string_view>

#include "engine.hxx"

int main(int /*argc*/, char* /*argv*/ [])
{
    std::unique_ptr<eng::engine, void (*)(eng::engine*)> engine(
        eng::create_engine(), eng::destroy_engine);

    engine->initialize("");

    bool continue_loop = true;
    while (continue_loop)
    {
        eng::event event;

        while (engine->read_input(event))
        {
            std::cout << event << std::endl;
            switch (event)
            {
                case eng::event::turn_off:
                    continue_loop = false;
                    break;
                default:
                    break;
            }
        }


        engine->render_triangle();

        engine->swap_buffers();
    }

    engine->uninitialize();

    return EXIT_SUCCESS;
}
